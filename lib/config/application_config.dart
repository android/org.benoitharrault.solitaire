import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:solitaire/cubit/activity/activity_cubit.dart';

import 'package:solitaire/ui/pages/game.dart';

import 'package:solitaire/ui/parameters/parameter_painter_layout.dart';

class ApplicationConfig {
  // activity parameter: skin
  static const String parameterCodeSkin = 'global.skin';
  static const String skinValueDefault = 'default';

  // activity parameter: layout
  static const String parameterCodeLayout = 'layout';
  static const String layoutValueFrench = 'french';
  static const String layoutValueGerman = 'german';
  static const String layoutValueEnglish = 'english';
  static const String layoutValueDiamond = 'diamond';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Solitaire',
    activitySettings: [
      // skin
      ApplicationSettingsParameter(
        code: parameterCodeSkin,
        values: [
          ApplicationSettingsParameterItemValue(
            value: skinValueDefault,
            isDefault: true,
          ),
        ],
      ),

      // layout
      ApplicationSettingsParameter(
        code: parameterCodeLayout,
        values: [
          ApplicationSettingsParameterItemValue(
            value: layoutValueFrench,
          ),
          ApplicationSettingsParameterItemValue(
            value: layoutValueGerman,
          ),
          ApplicationSettingsParameterItemValue(
            value: layoutValueEnglish,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: layoutValueDiamond,
          ),
        ],
        itemsPerLine: 2,
        customPainter: (context, value) => ParameterPainterLayout(
          context: context,
          value: value,
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
