import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:solitaire/models/activity/cell_location.dart';
import 'package:solitaire/models/activity/activity.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createEmpty(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      // Base data
      board: state.currentActivity.board,
      // Game data
      movesCount: state.currentActivity.movesCount,
      remainingPegsCount: state.currentActivity.remainingPegsCount,
      allowedMovesCount: state.currentActivity.allowedMovesCount,
    );
    // game.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);

    updateRemainingPegsCount(newActivity.countRemainingPegs());
    updateAllowedMovesCount(newActivity.countAllowedMoves());

    refresh();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void updatePegValue(CellLocation location, bool hasPeg) {
    state.currentActivity.board.cells[location.row][location.col].hasPeg = hasPeg;
    refresh();
  }

  void incrementMovesCount() {
    state.currentActivity.isStarted = true;
    state.currentActivity.movesCount++;
    refresh();
  }

  void updateRemainingPegsCount(int count) {
    state.currentActivity.remainingPegsCount = count;
    refresh();
  }

  void updateAllowedMovesCount(int count) {
    state.currentActivity.allowedMovesCount = count;
    if (count == 0) {
      state.currentActivity.isFinished = true;
    }

    refresh();
  }

  void move({
    required Activity currentActivity,
    required List<int> source,
    required List<int> target,
  }) {
    printlog('Move from $source to $target');
    final int sourceCol = source[0];
    final int sourceRow = source[1];
    final int targetCol = target[0];
    final int targetRow = target[1];

    final int middleRow = (sourceRow + ((targetRow - sourceRow) / 2)).round();
    final int middleCol = (sourceCol + ((targetCol - sourceCol) / 2)).round();

    updatePegValue(CellLocation.go(sourceRow, sourceCol), false);
    updatePegValue(CellLocation.go(targetRow, targetCol), true);
    updatePegValue(CellLocation.go(middleRow, middleCol), false);

    incrementMovesCount();
    updateRemainingPegsCount(currentActivity.countRemainingPegs());
    updateAllowedMovesCount(currentActivity.countAllowedMoves());
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
