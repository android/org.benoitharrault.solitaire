class GameData {
  static const Map<String, List<String>> templates = {
    'french': [
      '  ooo  ',
      ' ooooo ',
      'ooo·ooo',
      'ooooooo',
      'ooooooo',
      ' ooooo ',
      '  ooo  ',
    ],
    'german': [
      '   ooo   ',
      '   ooo   ',
      '   ooo   ',
      'ooooooooo',
      'oooo·oooo',
      'ooooooooo',
      '   ooo   ',
      '   ooo   ',
      '   ooo   ',
    ],
    'english': [
      '  ooo  ',
      '  ooo  ',
      'ooooooo',
      'ooo·ooo',
      'ooooooo',
      '  ooo  ',
      '  ooo  ',
    ],
    'diamond': [
      '    o    ',
      '   ooo   ',
      '  ooooo  ',
      ' ooooooo ',
      'oooo·oooo',
      ' ooooooo ',
      '  ooooo  ',
      '   ooo   ',
      '    o    ',
    ]
  };
}
