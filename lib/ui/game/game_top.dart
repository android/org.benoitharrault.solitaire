import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:solitaire/cubit/activity/activity_cubit.dart';
import 'package:solitaire/models/activity/activity.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Table(
          children: [
            TableRow(
              children: [
                Column(
                  children: [
                    Text(
                      '♟️ ${currentActivity.remainingPegsCount}',
                      style: const TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      currentActivity.allowedMovesCount.toString(),
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        color: Colors.green,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
