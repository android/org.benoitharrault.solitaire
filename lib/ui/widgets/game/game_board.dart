import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:solitaire/cubit/activity/activity_cubit.dart';
import 'package:solitaire/models/activity/board.dart';
import 'package:solitaire/ui/widgets/game/tile_widget.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Board board = activityState.currentActivity.board;
        final BoardCells cells = board.cells;

        final boardSize = activityState.currentActivity.boardSize;
        final double tileSize = (MediaQuery.of(context).size.width - 40) / boardSize;

        return Column(
          children: [
            Table(
              defaultColumnWidth: const IntrinsicColumnWidth(),
              children: [
                for (int row = 0; row < cells.length; row++)
                  TableRow(
                    children: [
                      for (int col = 0; col < cells[row].length; col++)
                        TableCell(
                          child: TileWidget(
                            tile: cells[row][col],
                            tileSize: tileSize,
                          ),
                        ),
                    ],
                  ),
              ],
            ),
          ],
        );
      },
    );
  }
}
