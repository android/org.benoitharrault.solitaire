import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:solitaire/config/application_config.dart';

import 'package:solitaire/cubit/activity/activity_cubit.dart';
import 'package:solitaire/models/activity/cell.dart';
import 'package:solitaire/models/activity/activity.dart';

class TileWidget extends StatelessWidget {
  const TileWidget({
    super.key,
    required this.tile,
    required this.tileSize,
  });

  final Cell tile;
  final double tileSize;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        if (tile.hasHole) {
          return render(
            currentActivity: currentActivity,
            activityCubit: BlocProvider.of<ActivityCubit>(context),
          );
        } else {
          return Image(
            image: AssetImage(
                'assets/skins/${currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin)}_board.png'),
            width: tileSize,
            height: tileSize,
            fit: BoxFit.fill,
          );
        }
      },
    );
  }

  Widget render({
    required Activity currentActivity,
    required ActivityCubit activityCubit,
  }) {
    List<Widget> stack = [
      hole(
        currentActivity: currentActivity,
        activityCubit: activityCubit,
      ),
    ];

    if (tile.hasPeg) {
      stack.add(draggable(
        currentActivity: currentActivity,
      ));
    }

    return Stack(
      alignment: Alignment.center,
      children: stack,
    );
  }

  Widget hole({
    required Activity currentActivity,
    required ActivityCubit activityCubit,
  }) {
    String image =
        'assets/skins/${currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin)}_hole.png';

    return DragTarget<List<int>>(
      builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return Image(
          image: AssetImage(image),
          width: tileSize,
          height: tileSize,
          fit: BoxFit.fill,
        );
      },
      onAcceptWithDetails: (DragTargetDetails<List<int>> source) {
        List<int> target = [tile.location.col, tile.location.row];
        // printlog('(drag) Pick from ' + source.toString() + ' and drop on ' + target.toString());
        if (currentActivity.board.isMoveAllowed(
          source: source.data,
          target: target,
        )) {
          activityCubit.move(
            currentActivity: currentActivity,
            source: source.data,
            target: target,
          );
        }
      },
    );
  }

  Widget draggable({
    required Activity currentActivity,
  }) {
    return Draggable<List<int>>(
      data: [tile.location.col, tile.location.row],

      // Widget when draggable is being dragged
      feedback: peg(
        currentActivity: currentActivity,
      ),

      // Widget to display on original place when being dragged
      childWhenDragging: Container(),

      // Widget when draggable is stationary
      child: peg(
        currentActivity: currentActivity,
      ),
    );
  }

  Widget peg({
    required Activity currentActivity,
  }) {
    String image =
        'assets/skins/${currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin)}_peg.png';

    return Image(
      image: AssetImage(image),
      width: tileSize,
      height: tileSize,
      fit: BoxFit.fill,
    );
  }
}
