import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:solitaire/config/application_config.dart';
import 'package:solitaire/data/game_data.dart';
import 'package:solitaire/models/activity/board.dart';
import 'package:solitaire/models/activity/cell.dart';
import 'package:solitaire/models/activity/cell_location.dart';

class ParameterPainterLayout extends CustomPainter {
  const ParameterPainterLayout({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    Color gridBackgroundColor = Colors.grey;

    switch (value) {
      case ApplicationConfig.layoutValueFrench:
        gridBackgroundColor = Colors.green;
        break;
      case ApplicationConfig.layoutValueGerman:
        gridBackgroundColor = Colors.orange;
        break;
      case ApplicationConfig.layoutValueEnglish:
        gridBackgroundColor = Colors.red;
        break;
      case ApplicationConfig.layoutValueDiamond:
        gridBackgroundColor = Colors.purple;
        break;
      default:
        printlog('Wrong value for size parameter value: $value');
    }

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3;

    // Mini grid
    final borderColor = Colors.grey.shade800;

    // Get board data from named templates
    final List<String>? template = GameData.templates[value];
    final BoardCells grid = [];

    int row = 0;
    template?.forEach((String line) {
      final List<Cell> gridLine = [];
      int col = 0;
      line.split("").forEach((String tileCode) {
        gridLine.add(tileCode == ' '
            ? Cell.none
            : Cell(
                location: CellLocation.go(row, col),
                hasHole: true,
                hasPeg: (tileCode == 'o'),
              ));
        col++;
      });
      row++;
      grid.add(gridLine);
    });

    final int gridWidth = grid.length;
    final int gridHeight = grid.first.length;
    paint.strokeWidth = 2;

    final double cellSize = (canvasSize * .9) / max(gridWidth, gridHeight);
    final double originX = (canvasSize - gridWidth * cellSize) / 2;
    final double originY = (canvasSize - gridHeight * cellSize) / 2;

    for (int row = 0; row < gridHeight; row++) {
      for (int col = 0; col < gridWidth; col++) {
        final Offset topLeft = Offset(originX + col * cellSize, originY + row * cellSize);
        final Offset bottomRight = topLeft + Offset(cellSize, cellSize);

        paint.color = Colors.white;
        paint.style = PaintingStyle.fill;

        if (grid[row][col].hasPeg) {
          paint.color = gridBackgroundColor;
          canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);
        }
        if (grid[row][col].hasHole) {
          paint.color = borderColor;
          paint.style = PaintingStyle.stroke;
          canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);
        }
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
