import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

class CellLocation {
  final int col;
  final int row;

  CellLocation({
    required this.col,
    required this.row,
  });

  factory CellLocation.go(int row, int col) {
    return CellLocation(col: col, row: row);
  }

  void dump() {
    printlog('$CellLocation:');
    printlog('  col: $col');
    printlog('  row: $row');
    printlog('');
  }

  @override
  String toString() {
    return '$CellLocation(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'col': col,
      'row': row,
    };
  }
}
