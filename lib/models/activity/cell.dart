import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:solitaire/models/activity/cell_location.dart';

class Cell {
  Cell({
    required this.location,
    required this.hasHole,
    required this.hasPeg,
  });

  final CellLocation location;
  bool hasHole;
  bool hasPeg;

  static Cell none = Cell(
    location: CellLocation.go(0, 0),
    hasHole: false,
    hasPeg: false,
  );

  void dump() {
    printlog('$Cell:');
    printlog('  location: $location');
    printlog('  hasHole: $hasHole');
    printlog('  hasPeg: $hasPeg');
    printlog('');
  }

  @override
  String toString() {
    return '$Cell(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'location': location,
      'hasHole': hasHole,
      'hasPeg': hasPeg,
    };
  }
}
