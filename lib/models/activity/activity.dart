import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:solitaire/config/application_config.dart';
import 'package:solitaire/models/activity/board.dart';
import 'package:solitaire/models/activity/cell.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.board,

    // Game data
    this.movesCount = 0,
    this.remainingPegsCount = 0,
    this.allowedMovesCount = 0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  final Board board;

  // Game data
  int movesCount;
  int remainingPegsCount;
  int allowedMovesCount;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: Board.createEmpty(),
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      board: Board.createNew(activitySettings: newActivitySettings),
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  bool get gameWon => isRunning && isStarted && isFinished;

  int get boardSize => board.boardSize;

  List<Cell> listRemainingPegs() {
    final List<Cell> pegs = [];

    final BoardCells cells = board.cells;
    for (int rowIndex = 0; rowIndex < cells.length; rowIndex++) {
      for (int colIndex = 0; colIndex < cells[rowIndex].length; colIndex++) {
        Cell tile = cells[rowIndex][colIndex];
        if (tile.hasPeg == true) {
          pegs.add(tile);
        }
      }
    }

    return pegs;
  }

  int countRemainingPegs() {
    return listRemainingPegs().length;
  }

  int countAllowedMoves() {
    int allowedMovesCount = 0;
    final List<Cell> pegs = listRemainingPegs();
    for (Cell tile in pegs) {
      final int row = tile.location.row;
      final int col = tile.location.col;
      final List<int> source = [col, row];
      final List<List<int>> targets = [
        [col - 2, row],
        [col + 2, row],
        [col, row - 2],
        [col, row + 2],
      ];
      for (List<int> target in targets) {
        if (board.isMoveAllowed(
          source: source,
          target: target,
        )) {
          allowedMovesCount++;
        }
      }
    }

    return allowedMovesCount;
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    board.dump();
    printlog('  Game data');
    printlog('    movesCount: $movesCount');
    printlog('    remainingPegsCount: $remainingPegsCount');
    printlog('    allowedMovesCount: $allowedMovesCount');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'board': board.toJson(),
      // Game data
      'movesCount': movesCount,
      'remainingPegsCount': remainingPegsCount,
      'allowedMovesCount': allowedMovesCount,
    };
  }
}
